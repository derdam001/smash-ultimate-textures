A repository holding PNG textures from Super Smash Bros. Ultimate. All files have been converted from the original NUTEXB files with [Switch-Toolbox](https://github.com/KillzXGaming/Switch-Toolbox).

This repository uses Git Large File Storage (LFS), in order to keep the repository sizes more manageable. See https://git-lfs.github.com/ for more information and how to get the client.
